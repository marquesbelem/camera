﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vThirdPersonCameraInput : MonoBehaviour {

	protected vThirdPersonCamera tpCamera;                // acess camera info
        [HideInInspector]
        public string customCameraState;                    // generic string to change the CameraState        
        [HideInInspector]
        public string customlookAtPoint;                    // generic string to change the CameraPoint of the Fixed Point Mode        
        [HideInInspector]
        public bool changeCameraState;                      // generic bool to change the CameraState        
        [HideInInspector]
        public bool smoothCameraState;                      // generic bool to know if the state will change with or without lerp  
        [HideInInspector]
        public bool keepDirection;                          // keep the current direction in case you change the cameraState

        [Header("Camera Settings")]
        public string rotateCameraXInput ="Mouse X";
        public string rotateCameraYInput = "Mouse Y";
		public float distance = 2.5f;
		public bool lockCamera = false;
		public float height = 1.4f;
	
	// Use this for initialization
	void Start () {
		tpCamera = FindObjectOfType<vThirdPersonCamera>();
        if (tpCamera) {
			tpCamera.SetMainTarget(this.transform);
			tpCamera.defaultDistance = distance;
			tpCamera.lockCamera = lockCamera;
			tpCamera.height = height;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		CameraInput();
	}
	
	void LateUpdate() {
		UpdateCameraStates();               // update camera states
	}
	
        #region Camera Methods

        protected virtual void CameraInput()
        {
            if (tpCamera == null)
                return;
            var Y = Input.GetAxis(rotateCameraYInput);
            var X = Input.GetAxis(rotateCameraXInput);

            tpCamera.RotateCamera(X, Y);

			tpCamera.defaultDistance = distance;
			tpCamera.lockCamera = lockCamera;
			tpCamera.height = height;

        // tranform Character direction from camera if not KeepDirection
        /*
        if (!keepDirection)
            cc.UpdateTargetDirection(tpCamera != null ? tpCamera.transform : null);
        // rotate the character with the camera while strafing        
        RotateWithCamera(tpCamera != null ? tpCamera.transform : null);            
        */

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
            tpCamera.ZoomOut();

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
            tpCamera.ZoomIn();


    }

        public virtual void UpdateCameraStates()
        {
            // CAMERA STATE - you can change the CameraState here, the bool means if you want lerp of not, make sure to use the same CameraState String that you named on TPCameraListData
            if (tpCamera == null)
            {
                tpCamera = FindObjectOfType<vThirdPersonCamera>();
                if (tpCamera == null)
                    return;
                if (tpCamera)
                {
                    tpCamera.SetMainTarget(this.transform);
                    tpCamera.Init();
                }
            }
			else{
				if (tpCamera.target != this.transform){
                    tpCamera.SetMainTarget(this.transform);
                    tpCamera.Init();
                }
			}
        }

		/*
        protected virtual void RotateWithCamera(Transform cameraTransform)
        {
            if (cc.isStrafing && !cc.lockMovement && !cc.lockMovement)
            {                
                cc.RotateWithAnotherTransform(cameraTransform);                
            }
        }
		*/

        #endregion
}
